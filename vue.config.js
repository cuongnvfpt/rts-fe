// const { config } = require("vue/types/umd");

module.exports = {
  devServer: {
    port: 3001,
    proxy: {
      '/api': {
        target: 'http://localhost:8080',
        ws: true,
        changeOrigin: true
      }
    }
  },
  transpileDependencies: [
    "vuetify"
  ]
  // publicPath: '/material-vue/'

}