import Course from "../../src/models/courses";
export default class Lesson {
    courseId = new Course();
    constructor(id,title, content, linkVideo, image,courseId){
        this.id = id
        this.title = title;
        this.content = content;
        this.linkVideo = linkVideo;
        this.image = image;
        this.courseId = courseId;
    }
}