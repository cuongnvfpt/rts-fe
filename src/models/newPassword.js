export default class newPassword{
    constructor(username, oldPassword, newPassword, confirmNewPassword){
      this.username = username;
      this.oldPassword = oldPassword;
      this.newPassword = newPassword;
      this.confirmNewPassword = confirmNewPassword;
    }
  }