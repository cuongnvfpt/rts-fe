// import Category from "../../src/models/category";
export default class course {
    // categoryId = new Category();
    constructor(id,title, thumbnail, content, sortDescription, coreExpert, price, created_by, create_date,categoryId,categoryName){
        this.id = id
        this.title = title;
        this.thumbnail = thumbnail;
        this.content = content;
        this.sortDescription = sortDescription;
        this.coreExpert = coreExpert;
        this.price = price;
        this.created_by = created_by;
        this.create_date = create_date;
        this.categoryId = categoryId;
        this.categoryName = categoryName
    }
}