export default class category {
  constructor(id, name, created_by) {
    this.id = id;
    this.name = name;
    this.created_by = created_by;
  }
}
