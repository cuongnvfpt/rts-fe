export default class User {
    constructor(id,username, password, role, phoneNumber, fullName,description) {
      this.id = id;
      this.username = username;
      this.password = password;
      this.role = role;
      this.phoneNumber = phoneNumber;
      this.fullName = fullName;
      this.description = description;
    }
  }
