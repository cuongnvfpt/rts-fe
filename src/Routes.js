import ChangePassword from "@/components/changePassword/changePassword";
import ForgotPassword from "@/components/forgotPassword/forgotPassword";
//Layout
import Layout from "@/components/layout/layout";
import LayoutCommon from "@/components/layout/layoutCommon";
//
import ManageAccount from "@/pages/dashboard/admin/account/manage";
import ManageCourses from "@/pages/dashboard/admin/courses/manage";
// Advisor
import AdvisorDashboard from "@/pages/dashboard/advisor/advisor";
import DetailAdvisor from "@/pages/dashboard/advisor/detail";
//Public feature end//
// DashBoard
import Dashboard from "@/pages/dashboard/dashboard";
import marketerBlog from "@/pages/dashboard/marketer/blog/manage";
import marketerCategory from "@/pages/dashboard/marketer/category/manage";
// Marketer
import marketerDashboard from "@/pages/dashboard/marketer/dashboard";
import marketerSlide from "@/pages/dashboard/marketer/slider/manage";
import Error from "@/pages/error/error";
//exam
import Exam from "@/pages/exam/Exam";
import CourseDetail from "@/pages/home/course/CourseDetail.vue";
// Public Feature
//course
import CourseList from "@/pages/home/course/courseList.vue";
import CourseRegister from "@/pages/home/course/courseRegister.vue";
import CourseRegisterConfirm from "@/pages/home/course/courseRegisterConfirm.vue";
import Home from "@/pages/home/home";
// import { ROLE } from "../src/constants/app.constant";
//----------Pages----------//
// //Home
import Icons from "@/pages/icons/icons";
//Learning
import Lesson from "@/pages/learn/lesson";
import Login from "@/pages/login/login";
import ManageLesson from "@/pages/personal/expert/lesson/manage";
//expert
import ListCourse from "@/pages/personal/expert/manageCourses/manage";
import ManageQuiz from "@/pages/personal/expert/quiz/manage";
import Profile from "@/pages/profile/userProfile";
import AboutUs from "@/pages/public/about-us/aboutUs";
import BlogDetail from "@/pages/public/blog/blogDetail";
import Blog from "@/pages/public/blog/blogList";
import BlogCreating from "@/pages/public/blog/createBlog";
import Contact from "@/pages/public/contact-us/contact";
import Vue from "vue";
import Router from "vue-router";







Vue.use(Router);
const router = new Router({
  mode: "history",
  routes: [{
      path: "/login",
      name: "Login",
      component: Login,

    },
    {
      path: "/forgotPassword",
      name: "ForgotPassword",
      component: ForgotPassword,
    },
    {
      path: "/changePassword",
      name: "ChangePassword",
      component: ChangePassword,
    },
    {
      path: "/",
      name: "LayoutCommon",
      component: LayoutCommon,
      children: [{
          path: "/",
          name: "Home",
          component: Home,
        },
        {
          path: "/courses",
          name: "CourseList",
          component: CourseList,

        },
        {
          path: "/courses/:id",
          name: "CourseDetail",
          component: CourseDetail,
          props: true,

        },
        {
          path: "/courses/:id/register",
          name: "CourseRegister",
          component: CourseRegister,
          props: true
        },
        {
          path: "/courses/:id/checkout",
          name: "CourseRegisterConfirm",
          component: CourseRegisterConfirm,
          props: true
        },
        {
          path: "/contact",
          name: "Contact",
          component: Contact,
        },
        {
          path: "/aboutus",
          name: "AboutUs",
          component: AboutUs,
        },
        {
          path: "/blog",
          name: "Blog",
          component: Blog,
        },
        {
          path: "/blog/:id",
          name: "BlogDetail",
          component: BlogDetail,
          props: true
        },
        {
          path: "/createblog",
          name: "BlogCreating",
          component: BlogCreating,
          props: true
        },
        {
          path: "/profile",
          name: "Profile",
          component: Profile,
        },
        {
          path: "/learn/:title",
          name: "Lesson",
          component: Lesson,
          props: true
        },
				{
					path: "/exam/:idExam/:userId/:courseId",
					name: "Exam",
					component: Exam,
					props: true
				},
      ],
    },
    //router Advisor
    {
      path: "/",
      name: "Layout",
      component: Layout,
      children: [{
          path: "/advisorDashboard",
          component: AdvisorDashboard,
        },
        {
          path: "/detail/:id",
          name: "DetailAdvisor",
          component: DetailAdvisor
        }
      ],
    },
    //router Marketer
    {
      path: "/",
      name: "Layout",
      component: Layout,
      children: [{
          path: "/marketerDashboard",
          component: marketerDashboard,
        },
        {
          path: "/manageBlog",
          component: marketerBlog,
        },
        {
          path: "/manageCategory",
          component: marketerCategory,
        },
        {
          path: "/manageSlide",
          component: marketerSlide,
        },
      ],
    },

    //admin
    {
      path: "/",
      name: "Layout",
      component: Layout,
      children: [{
          path: "dashboard",
          name: "Dashboard",
          component: Dashboard,
        },
        {
          path: "manageAccount",
          name: "ManageAccount",
          component: ManageAccount,
        },
        {
          path: "manageCourses",
          name: "ManageCourses",
          component: ManageCourses,
        },

      ],
    },
    // expert
    {
      path: "/",
      name: "Layout",
      component: Layout,
      children: [{
          path: "manageCourse",
          name: "ListCourse",
          component: ListCourse,
        },
        {
          path: "manageLesson",
          name: "ManageLesson",
          component: ManageLesson,
        },
        {
          path: "manageQuiz",
          name: "ManageQuiz",
          component: ManageQuiz,
        },
      ]
    },
    // edit
    {
      path: "/",
      name: "Layout",
      component: Layout,
      children: [{
          path: "dashboard",
          name: "Dashboard",
          component: Dashboard,
        },
        {
          path: "manageAccount",
          name: "ManageAccount",
          component: ManageAccount,
        },
        {
          path: "manageCourses",
          name: "ManageCourses",
          component: ManageCourses,
        },




      ],
    },
    {
      path: "/icons",
      name: "Icons",
      component: Icons,
    },
    {
      path: "*",
      name: "Error",
      component: Error,
    },
  ],
});

export default router;
// {
//   path: "/forgot-password",
//   name: "forgotPassword",
//   component: ForgotPassword,
//   beforeEnter(to, from, next) {
//     const user = firebase.auth().currentUser;
//     if (user) {
//       next({ name: "dashboard" });
//     } else {
//       next();
//     }
//   },
// }