import http from "../../http-common";
import authHeader from "../AuthHeader";


class categoryService {
  getAll() {
    return http.get(`/client/category/showAll`);
  }
  showCategoryByID(id) {
    return http.get(`/client/category/showCategory/${id}`);
  }
  create(data) {
    return http.post("marketer/addCategory", data, {headers: authHeader()});
  }
  delete(id,data) {
    return http.put(`marketer/deleteCategory/${id}`,data,{headers:authHeader()});
  }
  update(id, data) {
    return http.put(`marketer/updateCategory/${id}`, data,{headers:authHeader()});
  }
}
export default new categoryService();
