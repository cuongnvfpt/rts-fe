import http from "../../http-common";
import authHeader from "../AuthHeader";

class User {
  showUserById(id) {
    return http.get(`client/showUserById/${id}`, { headers: authHeader() });
  }
  update(data) {
    return http.put(`user/updateUser`, data, { headers: authHeader() });
  }

  showAvatarUser(id) {
    return http.get(`client/avatar/showAvatarUser/${id}`);
  }
  saveAvatarUser(id, data) {
    return http.put(`client/avatar/saveAvatarUser/${id}`, data, {headers: authHeader(), });
  }
  updateEmail(data) {
    return http.put(`user/updateEmail`, data, { headers: authHeader() });
  }
}

export default new User();
