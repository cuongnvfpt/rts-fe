import http from "../../http-common";
import authHeader from "../AuthHeader";


class LessonService {
    getAllLessonByCourseID(idCourse, idUser) {
        return http.get(`/user/lesson/getLessonByCourseIdForStudy/${idCourse}/${idUser}`, {
            headers: authHeader()
        });
    }
    getDetailLessonByID(idCourse, idUser, idLesson) {
        return http.get(`/user/lesson/getLessonByIdForStudy/${idCourse}/${idUser}/${idLesson}`, {
            headers: authHeader()
        })
    }

    updateLessonLearnStatus(idCourse, idUser, idLesson) {

        return http.post(`/user/lesson/successLesson/${idCourse}/${idUser}/${idLesson}`, {
            headers: authHeader()
        });
    }

    //Expert lesson CRUD
    getListLessonByCourseID(id) {
        return http.get(`/expert/findLessonByCourseId/${id}`, {
            headers: authHeader()
        });
    }
    addLesson(data) {
        return http.post(`/expert/addLesson`, data, {
            headers: authHeader(),
        });
    }
    getLessonDetail(id) {
        return http.get(`/expert/findLessonByLessonId/${id}`, {
            headers: authHeader()
        });
    }
    updateLesson(id, data) {
        return http.put(`/expert/updateLesson/${id}`, data, {
            headers: authHeader()
        });
    }
    deleteLesson(id) {
        return http.get(`/expert/deleteLesson/${id}`, {
            headers: authHeader(),
        });
    }
}


export default new LessonService();