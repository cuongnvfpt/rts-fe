import http from "../../http-common";
import authHeader from "../AuthHeader";


class ExamService {

    // getAllLessonByCourseID(idCourse) {
    //     return http.get(`/user/lesson/findLessonByCourseId/${idCourse}`, {
    //         headers: authHeader()
    //     });
    // }

    // getDetailLessonByID(id) {
    //     return http.get(`/user/lesson/findLessonByLessonId/${id}`, {
    //         headers: authHeader()
    //     })
    // }

    //Expert  CRUD

    getListExamByCourseID(id) {
        return http.get(`/expert/examListOfExpert/${id}`, {
            headers: authHeader()
        });
    }

    addExam(data) {
        return http.post(`/expert/addQuestion`, data, {
            headers: authHeader(),
						'Content-Type' : 'multipart/form-data'
        });
    }
    getExamDetail(id) {
        return http.get(`/expert/getExamDetailByExpert/${id}`, {
            headers: authHeader()
        });
    }

    updateExam(data) {
        return http.get(`/expert/updateExam/`, data, {
            headers: authHeader()
        });
    }

    deleteExam(id) {
        return http.get(`/expert/deleteExam/${id}`, {
            headers: authHeader(),
        });
    }
    submitResult(data) {
        return http.post(`/submitResult`, { data }, {
            headers: authHeader(),
        });
    }
}


export default new ExamService();