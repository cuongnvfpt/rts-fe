import http from "../../http-common";
import authHeader from "../AuthHeader";
class Slider {
  //Public
  show5SliderHome() {
    return http.get(`/client/sliders/showTop5Slider`);
  }
  showSliderHome(id) {
    return http.get(`client/sliders/showImageSlider/${id}`);
  }
  // Marketer
  showSlider() {
    return http.get(`marketer/showSlider`, { headers: authHeader() });
  }
  create(data) {
    return http.post(`marketer/addSliders`, data, { headers: authHeader() });
  }
  showSliderByID(id) {
    return http.get(`marketer/showSliders/${id}`,{ headers: authHeader()});
  }
  saveSliderInfo(id,data){
    return http.put(`marketer/updateSlider/${id}`,data,{ headers: authHeader()})
  }
  deleteSlider(id,data){
    return http.put(`marketer/deleteSliders/${id}`,data,{ headers: authHeader()})
  }
}
export default new Slider();
