export default function authHeader() {
  let user = JSON.parse(localStorage.getItem('user'));

  if (user && user.accessToken) {
    console.log(user.roles[0]);
    return { Authorization: 'Bearer ' + user.accessToken ,'Content-Type' : 'multipart/form-data'}; // for Spring Boot back-end
    // return { 'x-access-token': user.accessToken };       // for Node.js Express back-end
  } else {
    return {};
  }
}
