import http from "../http-common";

class AuthService {
    lostUser(data){return http.post(`/api/auth/lostUser`,data)}
    changePassword(data){return http.post(`api/auth/changePassword`,data)}
    login(user) {
        return http.post("/api/auth/signin", {
            username: user.username,
            password: user.password,

        }).then(response => {
            if (response.data.accessToken) {
                localStorage.setItem('user', JSON.stringify(response.data));
            }
            return response.data;
        })
    }
    logout() {
        localStorage.removeItem('user');
    }
    register(user) {
        return http.post("/api/auth/signup", {
            fullName: user.fullName,
            username: user.username,
            email: user.email,
            password: user.password,
            phoneNumber: user.phoneNumber
        });
    }
}
export default new AuthService();