import http from "../http-common";
import AuthHeader from "../services/AuthHeader";

class UserService{
    
    getPublicContent() {
        return http.get(`/`);
    }
    getAdminBoard() {
        return http.get("api/test/admin", { headers: AuthHeader });
    }
}
export default new UserService();