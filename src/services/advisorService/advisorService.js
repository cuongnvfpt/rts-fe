import http from "../../http-common";
import authHeader from "../AuthHeader";


class advisorService {
    getListRegisterCourseRequest() {
        return http.get(`/advisor/getListRegisterCourseForAdvisor`, {
            headers: authHeader(),
        });
    }

    updateCourseRegisterRequestStatus(user_id, course_id,data) {
        return http.put(`/advisor/updateStatus?user_id=${user_id}&course_id=${course_id}`,data, {
            headers: authHeader(),
        });
    }

    deleteCourseRegisterRequest(user_id, course_id) {
        return http.delete(`/advisor/userOutCourse?user_id=${user_id}&course_id=${course_id}`, {
            headers: authHeader(),
        });
    }

    // activeCourse/{id}
    updateCourseStatus(id,data) {
        return http.put(`/expert/activeCourse/${id}`,data, {
            headers: authHeader(),
        })
    }
}
export default new advisorService();