import http from "../../http-common";
import authHeader from "../AuthHeader";


class blogService {
    getTop4LastestBlog(){
        return http.get(`/client/blog/top4Blog`);
    }
    getAll(){
        return http.get(`/client/blog/list`);
    }
    getBlogByID(id){
        return http.get(`/client/blog/blogDetail?id=${id}`);
    }
    delete(id) {
        return http.delete(`marketer/deleteBlog?id=${id}`,{headers: authHeader()})
    }
    createBlog(data){
        return http.post(`/user/blog/addBlog`,data, {headers: authHeader()})
    }
    uploadImage(data){
        return http.post(`/user/blog/saveImageContent`,data, {headers: authHeader()})
    }
}
export default new blogService();
