import http from "../../http-common";
import authHeader from "../AuthHeader";

class CourseService {
  //expert-course
  allCourseExpertCreate(id) {
    return http.get(`expert/allCourseExpertCreate/${id}`, {
      headers: authHeader(),
    });
  }
  delete(id) {
    return http.delete(`expert/deleteCourse/${id}`, {
      headers: authHeader()
    });
  }
  create(data) {
    return http.post(`expert/addCourse`, data, {
      headers: authHeader(),
    });
  }
  saveCourseThumbnail(id, data) {
    var authHeader = new authHeader();
    return http.post(`expert/saveThumbnailCourse/${id}`, data, {
      headers: {
        authHeader: "authHeader",
        "Content-Type": "multipart/form-data",
      },
    });
  }
  update(id, data) {
    return http.put(`expert/updateCourse/${id}`, data, {
      headers: authHeader(),
    });
  }
  //public
  get4() {
    return http.get(`client/course/top4`);
  }

  getAll() {
    return http.get("client/course/allCourse");
  }

  getByID(id) {
    return http.get(`client/course/findCourse/${id}`);
  }

  getListLessonByCourseIDForShowing(idCourse) {
    return http.get(`client/course/getTitleLessonByCourse/${idCourse}`);
  }

  // /checkRegister/{idUser}/{idCourse}
  checkRegisteredCourse(user_id, course_id) {
    return http.get(`user/course/checkRegister/${user_id}/${course_id}`, {
      headers: authHeader(),
    });
  }

  courseRegister(data) {
    return http.post(`user/course/register`, data, {
      headers: authHeader(),
    });
  }
  getAllUserRegisteredCourse(id) {
    return http.get(`/user/course/showCourseOfUser/${id}`, {
      headers: authHeader(),
    });
  }
  getUserOfCourse(id) {
    return http.get(`advisor/showUserOfCourse/${id}`, {
      headers: authHeader(),
    });
  }
  getCourseOfUser(id) {
    return http.get(`user/course/showCourseOfUser/${id}`);
  }
}
export default new CourseService();