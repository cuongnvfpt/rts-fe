import http from "../../http-common";
import authHeader from "../AuthHeader";

class AccountService {
  getAll() {
    return http.get(`admin/showUser`, { headers: authHeader() });
  }
  getRole() {
    return http.get(`admin/getRole`, { headers: authHeader() });
  }
  showInfo(id) {
    return http.get(`client/showUserById/${id}`,{ headers: authHeader()});
  }
  showUserByID(id) {
    return http.get(`admin/showUserById/${id}`,{ headers: authHeader()});
  }
  deleteUser(id){
    return http.delete(`admin/deleteUser/${id}`,{ headers: authHeader()})
  }
  getRegisteredList() {
    return http.get(`advisor/allRegistered`, { headers: authHeader() });
  }
  getRegisteredByID(id) {
    return http.get(`advisor/getRegisteredById/${id}`, {
      headers: authHeader(),
    });
  }
  updateRole(data) {
    return http.put(`admin/updateRole`, data, { headers: authHeader() });
  }
}

export default new AccountService();
