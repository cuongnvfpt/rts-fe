// import authHeader from "../services/AuthHeader";

export const COOKIES = {
    ROLE: "role",
    USER: "user",
    TOKEN:"token",
}
export const ROLE = {
    GUEST : "GUEST",
    ADMIN : "ROLE_ADMIN",
    USER : "ROLE_USER",
    EXPERT: "ROLE_EXPERT",
    ADVISOR: "ROLE_ADVISOR",
    MARKETER: "ROLE_MARKETER",
}
export const UrlImg = {
    URL_SLIDER: "http://localhost:8082/client/sliders/showSlider",
    URL_IMAGE : "http://localhost:8082/client/sliders/showImageSlider/",
    URL_AVATAR_USER: "http://localhost:8082/client/avatar/showAvatarUser/",
}
