export const  DispatchAction = {
        LOGIN : "auth/login",
        REGISTER : "auth/register",
        LOGOUT : "auth/logout",
}
export const Getter= {
    USER : "auth/user",
    ROLE : "auth/role",
}