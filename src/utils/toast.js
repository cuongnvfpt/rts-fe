import Vue from "vue";
import Toast from "vue-toastification";
// Import the CSS or use your own!
import "vue-toastification/dist/index.css";

const options = {
    // You can set your default options here
};

Vue.use(Toast, options);

function $Toast() { }
$Toast.prototype.success = function (title, timeout = 3000) {  
  Vue.$toast.success(title, { timeout });
};
$Toast.prototype.error = function (title, timeout = 3000) {
  Vue.$toast.error(title, { timeout });
};
$Toast.prototype.warning = function (title, timeout = 3000) {
  Vue.$toast.warning(title, { timeout });
};
$Toast.prototype.warning = function (title, timeout = 3000) {
  Vue.$toast.info(title, { timeout });
};
window.$toast = new $Toast();