import Vue from 'vue'
import App from './App.vue'
import router from './Routes'
import store from './store/index'
import vuetify from './plugins/vuetify'
import VueEditor from "vue2-editor";

// import '@/interception';
import * as VueGoogleMaps from 'vue2-google-maps';
// import VeeValidate from 'vee-validate';
import "vue-toastification/dist/index.css";
import Vuelidate from 'vuelidate';
import JwPagination from 'jw-vue-pagination';
import "./utils/toast";




Vue.use(VueEditor);
Vue.use(Vuelidate);
// Vue.use(VeeValidate);
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyB7OXmzfQYua_1LEhRdqsoYzyJOPh9hGLg',
  },
});
Vue.config.productionTip = false

Vue.config.productionTip = false
Vue.component('jw-pagination', JwPagination);


new Vue({
  vuetify,
  router,
  render: h => h(App), store
}).$mount('#app')
