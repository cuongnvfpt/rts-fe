import { ROLE } from "../constants/app.constant";
import http from "../http-common";
import authHeader from "../services/AuthHeader";
import AuthService from '../services/AuthService';

const user = JSON.parse(localStorage.getItem('user'));
const initialState = user
  ? { status: { loggedIn: true }, user }
  : { status: { loggedIn: false }, user: null };
export const getter = {
  isAdmin: initialState => initialState.ROLE == ROLE.ADMIN

}
const state = {
  user: null,
  posts: null,
}
const actions = {

}
const getters = {

}
const mutations = {
  loginSuccess(state, user) {
    state.status.loggedIn = true;
    state.user = user;
  },
  loginFailure(state) {
    state.status.loggedIn = false;
    state.user = null;
  },
  logout(state) {
    state.status.loggedIn = false;
    state.user = null;
  },
  registerSuccess(state) {
    state.status.loggedIn = false;
  },
  registerFailure(state) {
    state.status.loggedIn = false;
  }
}
export default {
  state,
  actions,
  getters,
  mutations
}
export const auth = {
  namespaced: true,
  state: initialState,
  actions: {
    login({ commit }, user) {
      return AuthService.login(user).then(
        user => {
          commit('loginSuccess', user);
          return Promise.resolve(user);
        },
        error => {
          commit('loginFailure');
          return Promise.reject(error);
        }
      );
    },
		sendResult (data) {
			return http.post(`/submitResult`, data, {
				headers: authHeader()
			});
		}
  },
  mutations: {
    submitResult(state, user) {
      state.status.loggedIn = true;
      state.user = user;
    }
  }
};
