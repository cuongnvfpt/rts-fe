const state = {
    isLoading: 0
}

const mutations = {
    START_LOADING: (state) => state.isLoading++,
    STOP_LOADING: (state) => state.isLoading--
}

const actions = {
    START_LOADING: ({ commit }) => commit('START_LOADING'),
    STOP_LOADING: ({ commit }) => commit('STOP_LOADING')
}
const getters = {

}

export default {
    state,
    actions,
    getters,
    mutations
}